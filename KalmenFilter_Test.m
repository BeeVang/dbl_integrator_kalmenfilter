% KalmenFilter_Test.m
% Last Modified: Nov. 7, 2020 by Bee Vang
% Setup the test environment for a Kalman Filter on a double integrator
% system

%% Clean workspace
close all; clear all; clc;

%% Setup parameters
% Define the dynamical system
% x_dot = F*x + B*u + w
% z = H*x + v
% where w = mvnrnd(zero(6,1),Q) and v = mvnrnd(zeros(6,1),R)
F = [zeros(3) eye(3);zeros(3, 6)];
B = [zeros(3);eye(3)];
% H = [eye(3) zeros(3)]; % Only measure position
H = [eye(3) zeros(3)];
Q = [2*eye(3) zeros(3);zeros(3) 2*eye(3)]; % Constant proccess noise covar; no relationship between states
R = .001*eye(3); % position measurement noise
P0 = eye(6);
% Define some initial parameters
m = 10; % Mass
TFinal = 10;
x0_pos = randn(3,1); % Random initial position
x0_vel = randn(3,1); % Random initial velocity
x0 = [x0_pos;x0_vel]; % Define the initial state vector
x0 = [x0;x0]; % Double since we have real and nosiy states
x0_hat = [5*x0_pos;5*x0_vel];% + mvnrnd(zeros(6,1),Q)'; % Init estimate with some noise
x0 = [x0;x0_hat;P0(:)];

%% Simulation
control = @(t,x) [.5;.5;-.5];
closedLoop = @(t, x) linear_model(x, control(t,x), F, B, H, Q, R);
optsOde=odeset('OutputFcn',@odeplot,'MaxStep',0.01);
figure;
[t,x]=ode45(closedLoop,[0 TFinal], x0, optsOde);

%% Analysis and Plots
% Plot the states
figure;
% x
subplot(2,3,1);
plot(t,x(:,1),'-');
hold on
plot(t,x(:,7),'--');
plot(t,x(:,13),'o')
title('x')
legend('Ideal','Noise','Est.')
% y
subplot(2,3,2);
plot(t,x(:,2),'-');
hold on
plot(t,x(:,8),'--');
plot(t,x(:,14),'o')
title('y')
legend('Ideal','Noise','Est.')
% z
subplot(2,3,3);
plot(t,x(:,3),'-');
hold on
plot(t,x(:,9),'--');
plot(t,x(:,15),'o')
title('z')
legend('Ideal','Noise','Est.')
% x_dot
subplot(2,3,4);
plot(t,x(:,4),'-');
hold on
plot(t,x(:,10),'--');
plot(t,x(:,16),'o')
title('x dot');
legend('Ideal','Noise','Est.')
% y_dot
subplot(2,3,5);
plot(t,x(:,5),'-');
hold on
plot(t,x(:,11),'--');
plot(t,x(:,17),'o')
title('y dot');
legend('Ideal','Noise','Est.')
% z_dot
subplot(2,3,6);
plot(t,x(:,6),'-');
hold on
plot(t,x(:,12),'--');
plot(t,x(:,18),'o')
title('z dot');
legend('Ideal','Noise','Est.')