function [x] = packState(x_pos, x_vel, x_noise, v_noise, x_est, v_est, P)
% Last edited: Nov 7 2020 by Bee Vang
% Pack the states into a [54x1] vector

x = [x_pos; x_vel; x_noise; v_noise; x_est; v_est; P(:)];
end

