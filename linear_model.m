function [dx] = linear_model(x,u,F,B,H,Q,R)
% INPUTS:
%   x := state vector [true pos; true vel; noisey pos; noisey vel; est pos; est vel, P]
%   u := control input handle
%   F := state transition matrix
%   B := control input matrix
%   H := measurement matrix; ie y(x) = Cx + w where w is the measurement noise
%   Q := process noise covariance
%   R := measurement noise covariance
% OUTPUTS:
%   dx := new states, [6x1] vector
x_true = x(1:6);

dx = F*x_true + B*u; % True states
% mvnrnd(mu,sigma) computes the multivariant normal distribution
dx(7:12) = dx(1:6) + mvnrnd(zeros(6,1),Q)'; % add random proccess noise

% Update estimates
dx(13:54) = bucy_kalmen_filter(0, x, u, F, B, H, Q, R);
end