function [dx_hat] = bucy_kalmen_filter(t,x,u,F,B,H,Q,R)
% Last edited: Nov. 7, 2020 by Bee Vang
% Implement Bucy-Kalmen Filter for LTI system
% x_dot = F*x + B*u + w
% z = H*x + v
% where w = mvnrnd(zero(6,1),Q) and v = mvnrnd(zeros(6,1),R)
% INPUTS:
%   t := current evaluation time
%   x := state vector [true pos; true vel; noisey pos; noisey vel; est pos; est vel]
%   u := control input handle
%   F := state transition matrix
%   B := control input matrix
%   H := measurement matrix; ie y(x) = Cx + w where w is the measurement noise
%   Q := process noise covariance
%   R := measurement noise covariance
% OUTPUTS:
%   dx_hat := the estimated states
[~,~,x_noise,v_noise,x_est,v_est,P] = unpackState(x);

% Sample
z = H*[x_noise;v_noise] + mvnrnd(zeros(3,1),R)';

% Gain update
K = P*H'/R;

% estimate dynamics
dx_hat = F*[x_est;v_est] + B*u + K*(z-H*[x_est;v_est]);
temp_dP_dt = F*P + P*F' + Q - K*R*K';
dx_hat(7:42) = temp_dP_dt(:);
end

