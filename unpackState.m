function [x_true, v_true, x_noise, v_noise, x_est, v_est, P] = unpackState(x)
% Last edited: Nov. 7, 2020 by Bee Vang
% Extract the states from the state vector
% INPUTS:
%   x := state vector [True x_pos, True x_vel; noisy x_pos; noisy x_vel;
%       est x_pos; est x_vel; P estimation covar] [54x1] state vector
% OUTPUTS:
%   x_true := [3x1] true position
%   v_true := [3x1] true velocity
%   x_noise := [3x1] noisey position
%   v_noise := [3x1] noisey velocity
%   x_est := [3x1] estimated position
%   v_est := [3x1] estimated velocity
%   P := [6x6] symmetric estimation covariance

x_true = x(1:3);
v_true = x(4:6);
x_noise = x(7:9);
v_noise = x(10:12);
x_est = x(13:15);
v_est = x(16:18);
P = reshape(x(19:54),6,6);
end

